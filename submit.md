---
title: Submit
layout: page
---

[&larr; Go home](/)

Submit to the 1kB Club

1. Put your website through [GTmetrix](https://gtmetrix.com)
2. [Open a PATCH](https://lists.sr.ht/~bt/1kb-club)

Use the template below when creating your site file (size in bytes):

```
---
pageurl: domain.com
size: 300
---
```