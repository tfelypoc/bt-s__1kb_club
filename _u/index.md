---
layout: default
size: 0
permalink: /u/index.html
---

[&larr; Go home](/)

Get your own `user` 1kB Club sub-folder domain!

1. Fork the 1kB repo
2. Add a new file under `_u`
3. Setup the file settings based on the template below
4. Save the file as `yourname.md`
5. [Open a PATCH here](https://lists.sr.ht/~bt/1kb-club)

Use the template below when creating your site file (size in bytes):

```
---
layout: post
name: Bradley Taunt
blurb: I'm a UX designer from Canada.
pageurl: tdarb
email: brad@bt.ht
size: 646
---
```
